/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul2;

/**
 *
 * @author Asus
 */
public class MasyarakatSekitar extends Penduduk {
    private String nomor;
    private String nama, tempatTanggalLahir ;
    
    public MasyarakatSekitar(){    
    }
    public MasyarakatSekitar(String nomor, String nama, String tanggalLahir) {
        this.nama = nama;
        this.nomor = nomor;
        this.tempatTanggalLahir = tanggalLahir;
    }
    public void setNomor(String nomor){
        this.nomor = nomor;
    }
    public String getNomor(){
        return nomor;
    }
    @Override
    public double hitungIuran(){
        double tahunMasuk = Double.parseDouble(nomor.substring(0,2));
        return tahunMasuk*100;
    }
    @Override
    public String toString(){
        String iuran= String.valueOf(Math.round(hitungIuran()));
        return nomor+ "    |\t"+nama+"\t\t\t|    "+tempatTanggalLahir+"\t| Rp"+iuran+"\t|";    
    }
}
