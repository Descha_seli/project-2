/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul2;

/**
 *
 * @author Asus
 */
public class Main {
      public static void main(String[] args) {
   
        UKM kegiatan = new UKM("Seni Lukisan");
        Mahasiswa ketua = new Mahasiswa("195311001", "Seraphine Alana", "27/04/01");
        Mahasiswa sekretaris = new Mahasiswa("195311245", "Jacob Narayan", "17/03/01");

        kegiatan.setKetua(ketua);
        kegiatan.setSekretaris(sekretaris);

        Mahasiswa member1 = new Mahasiswa("195311543", "Stephani Anggita", "06/09/01");
        Mahasiswa member2 = new Mahasiswa("195311221", "Rara Sekar Ayu", "13/08/01");
        MasyarakatSekitar member3 = new MasyarakatSekitar("194575", "Londo", "14/05/00");
        MasyarakatSekitar member4 = new MasyarakatSekitar("194532", "Yudi", "15/05/00");

        Penduduk member[] = new Penduduk[4];
        member[0] = member1;
        member[1] = member2;
        member[2] = member3;
        member[3] = member4;

        double totalIuran = 0;

        System.out.println("Nama UKM            :" + kegiatan.getNama());
        System.out.println("Nama Ketua UKM      :" + ketua.getNama());
        System.out.println("NIM                 :" + ketua.getNim());
        System.out.println("Tanggal Lahir       :" + ketua.getTTL());
        System.out.println();
        System.out.println("Nama Sekretaris UKM :" + sekretaris.getNama());
        System.out.println("NIM                 :" + sekretaris.getNim());
        System.out.println("Tangggal Lahir      :" + sekretaris.getTTL());
        System.out.println();

        System.out.println("Daftar Anggota    : \n");
        System.out.println("==============================================================================================");
        System.out.printf("%-5s", "No |");
        System.out.printf("%8s", "NIM");
        System.out.printf("%8s", " | ");
        System.out.printf("%4s", "\t\tNama");
        System.out.printf("\t\t| ");
        System.out.printf("%5s", "Tanggal Lahir");
        System.out.printf(" | ");
        System.out.printf("%10s", "Iuran");
        System.out.printf("\t|\n");
        System.out.printf("---|---------------|-------------|--------------|----------------------------------------|\n");

        for (int i = 0; i < member.length; i++) {
            System.out.println(i + 1 + "  |\t" + member[i].toString());
            totalIuran += member[i].hitungIuran();

            System.out.println("-----------------------------------------------------------------------------------------");
            System.out.printf("\t\t\t\t\t\tTotal Iuran    | Rp " + Math.round(totalIuran));
            System.out.println("\t|");
            System.out.println("========================================================================================");

        }

    }

}
