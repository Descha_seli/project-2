/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum2;

 abstract public class Penduduk {
    protected String nama, tempatTanggalLahir ;
    
    public Penduduk(){    
    }
    public Penduduk(String nama, String tempatTanggalLahir){
        this.nama = nama;
        this.tempatTanggalLahir =tempatTanggalLahir;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return nama;
    }
    public void setTTL(String ttl){
        this.tempatTanggalLahir=ttl;
    }
    public String getTTL(){
        return tempatTanggalLahir;
    }
    abstract public double hitungIuran(); 
}
